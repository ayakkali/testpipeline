﻿using System;
using EnCryptDecrypt;

namespace ConsoleTestApp
{
    public class Program
    {
        bool[] b = new bool[3];
        int count = 0;
        static void Main(string[] args)
        {
            string jsonData = "{\"id\":0,\"EmployeeId\":3244,\"FirstHalf\":null,\"SecondHalf\":null,\"Comments\":null,\"Date\":\"2017-08-07 12:34:56:948\",\"EmailRecipients\":[ayakkali@spiderlogic.com, uday@spiderlogic.com],\"CreatedBy\":0,\"CreatedOn\":\"2017-08-07T12:35:58:938\",\"UpdatedBy\":null,\"UpdatedOn\":null}";

            var encryptedData = CryptorEngine.Encrypt(jsonData, true);
            //Console.WriteLine(encryptedData);

            //var en = "E/08D42n6YV3bst6EtzLEDrTeAiouRqUSn/dKcyjlU4mFWOiEFKBClCcWtQORZih3nbBU7WsPxe71nnFF0NuxdyAwwjF38Q6MUlFiXpHwfZm0XZ729tCRqWklzykgmJqMtKvAjuJZXI/RbMFAkdCVlMmQ48otlS5RF52bU/f+tiMXtSEcdv9Jl0Ja/aqjzi+Y0tTMhO5ZDZuz/Fb2CowZqo60wR4oBZ4lhHvi8BU8Cx1GnA1Zdz2kDnQVOjDAJu4nFwREm8IrYMXiEmvdN8pXVoQ3Y7dtyxVSplIJUoEFfM2w+TBtwfFfnSCjC9DUhQGvYnJo8ofY3zcUVsdbEKu/LIr2OywY0dX7gMV4ALkSg1hyuO9KwRAXA==";
            var en = "tTzKqECFUWdWgjQzeLtrgsstHm7i+dXgjLUSzj1UBn8rmy5ithZH9exOM+sdQmMu++6h4KX2R7J8a4boV5lCOHSFhP+Un5wLaclsf18HKn532NABQM108djf5z3k5617AnQC8mTssQdEHwif9alQLgwEn4ornBQqQEHScw5banqNPV9AXwNSsVKgYjbpURSpCa8fqVQxuxvdn58wdL1xak0b0g4ZER5XJVOGIgcTkrH0i7dEt3Fke82GJbfD3RjJ85yGDTLuKPK61nMSinzpk76Y3AT9h2fIp7OwS6LtDzAXiEmvdN8pXQHvFXURirV+DqWG5NmUjY6CHpD2C/EfIg==";
            Console.WriteLine(CryptorEngine.Decrypt(en, true));

            //ExcelHelper excelHelper = new ExcelHelper();
            //excelHelper.GenerateExcel();
            //excelHelper.CreateExcelFile(@"F:\FileName.xls");

            //var timeWindow = "16:00 - 20:00".Split('-');
            //var date = "06/12/2017";

            //var startDate = Convert.ToDateTime(date);
            //Console.WriteLine(startDate.AddHours(Int32.Parse(timeWindow[0].Split(':')[0])));
            //Console.WriteLine(startDate.AddHours(Int32.Parse(timeWindow[1].Split(':')[0])));

            //SyncAwaitExample sae = new SyncAwaitExample();
            //sae.callAsync();

            //int a = 5;
            //int b = 0, c = 0;

            //a = method(a, b, ref c);

            //Console.WriteLine(a + " " + b + " " + c);
            Console.WriteLine("Main method");
            //Console.ReadLine();
            //DerivedClass objDC = new DerivedClass();
            //objDC.Method1();

            //BaseClass baseClass = new BaseClass();
            //baseClass.Method1(12);

            //BaseClass b = new DerivedClass();
            //b.Method1(14);
            Console.ReadLine();
            //Console.WriteLine(" END Main method");
        }

        private static int method(int x, int p, ref int k)
        {

            p = ++x + x*x++;
            k = x*x + p;
            return 0;
        }

    }

    // Base class
    public class BaseClass
    {
        public void Method1(double x)
        {
            Console.Write("Base Class Method" + x);
        }
    }
    // Derived class
    public class DerivedClass : BaseClass
    {
        public void Method1(int y)
        {
            Console.Write("Derived Class Method"+y);
        }
    }
}
